# 3dvj

## what?

This is a performance system for anaglyphic (red-blue) 3d visuals. 

you can create virtual 3d environments in realtime with…

*	an arbitrary number of 3d objects that you can rotate through using OSC input

*	an arbitrary number of different 3d scenes that you can advance through using OSC input

*	four cameras per scene, switchable using OSC input

*	four bands of audio analysis can be scripted to control objects in each scene

*	textures created from real-time camera input and controllable (2d) video effects

*	a custom hardware controller for mouseless/keyboardless control over everything.

##	hold up–

this is still under active development and also super duper pre-alpha so don't get too comfortable.

I also haven't written any documentation yet. 

## how?

In software, I’m using the unity3d game engine to control the 3d environment, quartz composer to merge two camera feeds into anaglyph 3d, coge to process realtime 2d video effects, and syphon to route video feeds around my computer. 

for hardware, I want to make a wooden box with an anodized aluminum front panel, into which I’ll laser cut openings and labels. Inside the box, I want to use a UMC32+M midi brain from Hale Microsystems, four breakout boards, and a variety of 40 inputs (including potentiometers, linear faders, a light sensor, pressure pads, buttons, and switches)

I already have the technology.
The following crappy video was made using a previous version of this project. It’s a merged anaglyphic 3d video (ie you can see it with 3d glasses) of a field of tranfsorming spheres in front of a plane textured with a (live) video of me manipulating a potentiometer and a photoresistor in order to control those spheres.

<iframe src="http://player.vimeo.com/video/63160078" width="500" height="375" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

## parts

<table style="border-collapse:collapse;font-size:10px;">
<caption style="caption-side:top;font-weight:bold;text-align:center;font-size:12px;">Costs</caption>
	<tr>
		<td style="border:1px solid #D6D6D6;text-align:center;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">0.0</td>
		<td style="border:1px solid #D6D6D6;text-align:center;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">thing</td>
		<td style="border:1px solid #D6D6D6;text-align:center;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">source</td>
		<td style="border:1px solid #D6D6D6;text-align:center;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">#</td>
		<td style="border:1px solid #D6D6D6;text-align:center;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">$ each</td>
		<td style="border:1px solid #D6D6D6;text-align:center;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">$$$$</td>
	</tr>
	<tr>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">controller parts</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">umc32+m</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">hale microsystems</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">1.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">89.95</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">89.95</td>
	</tr>
	<tr>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">controller parts</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">sc8-nc-2pk</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">hale microsystems</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">2.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">17.95</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">35.9</td>
	</tr>
	<tr>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">controller parts</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">ribbon cable pack</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">hale microsystems</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">2.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">5.95</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">11.9</td>
	</tr>
	<tr>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">controller parts</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">resistors</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">saic</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">32.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">2.5</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">80.0</td>
	</tr>
	<tr>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">controller parts</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">usb cable</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">saic</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">1.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">2.2</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">2.2</td>
	</tr>
	<tr>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">enclosure parts</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">anodized aluminum sheet</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">inventables</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">2.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">25.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">50.0</td>
	</tr>
	<tr>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">enclosure parts</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">wooden box</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">saic</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">1.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">30.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">30.0</td>
	</tr>
	<tr>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">fabrication costs</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">laser cutter time (45min slot)</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">saic</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">2.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">5.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">10.0</td>
	</tr>
	<tr>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">other materials</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">3d glasses</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">amazon</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">50.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">0.5</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;">25.0</td>
	</tr>
	<tr>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">0.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">0.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;font-weight:bold;">0.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;">0.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;">0.0</td>
		<td style="border:1px solid #D6D6D6;text-align:left;vertical-align:top;color:#000000;background-color:#E6E6E6;">334.95</td>
	</tr>
</table>