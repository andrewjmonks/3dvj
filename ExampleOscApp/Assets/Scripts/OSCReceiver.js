//You can set these variables in the scene because they are public 
public var RemoteIP : String = "127.0.0.1";
public var SendToPort : int;
public var ListenerPort : int = 57130;
private var controller : Transform; 
private var handler : Osc;
static var messages : Array = [0.0,0.0,0.0,0.0,0.0,1.0];
private var nextSceneGo : boolean;
private var prevSceneGo : boolean;
private var whichCam : GameObject;
private var newCam : GameObject;
static var cam1 : GameObject;
static var cam2 : GameObject;
static var cam3 : GameObject;
static var cam4 : GameObject;
static var leftCam : GameObject;
static var rightCam : GameObject;
static var dupe : GameObject;
private var eyeWidth : float;
// private var colorMode : int;
// private var colorUp : boolean;
// private var colorDown : boolean;
private var nextObj : boolean;
private var prevObj : boolean;
static var objAr : Array;

public var meshes : Mesh[];
static var whichMesh : int = 0;


public function Start () {
	objAr = GameObject.FindGameObjectsWithTag("gogogo");
	loadObj(whichMesh);

	colorMode = 0;
	dupe = GameObject.Find("Dupe");
	cam1 = GameObject.Find("_Stereocam1");
	cam2 = GameObject.Find("_Stereocam2");
	cam3 = GameObject.Find("_Stereocam3");
	cam4 = GameObject.Find("_Stereocam4");
	leftCam = GameObject.Find("left");
	rightCam = GameObject.Find("right");
	whichCam = cam1;
	newCam = cam1;

	prevSceneGo = false;
	nextSceneGo = false;
	//Initializes on start up to listen for messages
	//make sure this game object has both UDPPackIO and OSC script attached
	var udp : UDPPacketIO = GetComponent("UDPPacketIO");
	udp.init(RemoteIP, SendToPort, ListenerPort);
	handler = GetComponent("Osc");
	handler.init(udp);
			
	handler.SetAddressHandler("/3d/mod1", ListenEvent);
	handler.SetAddressHandler("/3d/mod2", ListenEvent);
	handler.SetAddressHandler("/3d/mod3", ListenEvent);
	handler.SetAddressHandler("/3d/mod4", ListenEvent);
	handler.SetAddressHandler("/3d/scenenext", ListenEvent);
	handler.SetAddressHandler("/3d/sceneprev", ListenEvent);
	handler.SetAddressHandler("/3d/cam1", ListenEvent);
	handler.SetAddressHandler("/3d/cam2", ListenEvent);
	handler.SetAddressHandler("/3d/cam3", ListenEvent);
	handler.SetAddressHandler("/3d/cam4", ListenEvent);
	handler.SetAddressHandler("/3d/obj1next", ListenEvent);
	handler.SetAddressHandler("/3d/obj1prev", ListenEvent);
	// handler.SetAddressHandler("/3d/obj2next", ListenEvent);
	// handler.SetAddressHandler("/3d/obj2prev", ListenEvent);
	handler.SetAddressHandler("/3d/width", ListenEvent);
}

public function Update() {
	if(nextSceneGo == true) {
		nextSceneGo = false;
		nextScene();
	}
	if(prevSceneGo == true) {
		prevSceneGo = false;
		prevScene();
	}
	if(whichCam != newCam){
		switchCam(newCam);
		whichCam = newCam;
	}
	// if(colorUp == true) {
	// 	colorUp = false;
	// 	colorMode = Mathf.Clamp(colorMode+1,0,3); setMode(colorMode);
	// }
	// if(colorDown == true) {
	// 	colorDown = false;
	// 	colorMode = Mathf.Clamp(colorMode-1,0,3); setMode(colorMode);
	// }
	
	if (nextObj == true) {
		nextObj = false;
		loadObj(Mathf.Clamp(whichMesh + 1,0,meshes.length - 1));
		whichMesh = Mathf.Clamp(whichMesh + 1,0,meshes.length - 1);
	}
	if (prevObj == true) {
		prevObj = false;
		loadObj(Mathf.Clamp(whichMesh - 1,0,meshes.length - 1));
		whichMesh = Mathf.Clamp(whichMesh - 1,0,meshes.length - 1);
	}

	// eyeFix();
}

public function ListenEvent(oscMessage : OscMessage) : void {	
	switch(oscMessage.Address){
		case "/3d/mod1":		i = 0; messages[i] = oscMessage.Values[0]; break;
		case "/3d/mod2":		i = 1; messages[i] = oscMessage.Values[0]; break;
		case "/3d/mod3":		i = 2; messages[i] = oscMessage.Values[0]; break;
		case "/3d/mod4":		i = 3; messages[i] = oscMessage.Values[0]; break;
		case "/3d/scenenext":	if(oscMessage.Values[0] == 1){ nextSceneGo = true;} break;
		case "/3d/sceneprev":	if(oscMessage.Values[0] == 1){ prevSceneGo = true;} break;
		case "/3d/cam1":		newCam = cam1; break;
		case "/3d/cam2":		newCam = cam2; break;
		case "/3d/cam3":		newCam = cam3; break;
		case "/3d/cam4":		newCam = cam4; break;
		case "/3d/obj1next":	if(oscMessage.Values[0] == 1){ nextObj = true; } break;
		case "/3d/obj1prev":	if(oscMessage.Values[0] == 1){ prevObj = true; } break;
		// case "/3d/obj2next":	break;
		// case "/3d/obj2prev":	break;
		case "/3d/width":		i = 4; messages[i] = oscMessage.Values[0] - 0.5; break;
	}	
} 
function eyeFix() {
	leftCam.transform.localPosition = Vector3(0 - messages[4] * 2.0, 0, 0);
	rightCam.transform.localPosition = Vector3(messages[4] * 2.0, 0, 0);
}

function switchCam(n) {
	leftCam.transform.parent = null;
	rightCam.transform.parent = null;
	leftCam.transform.position = Vector3(n.transform.position.x - messages[4] * 2.0, n.transform.position.y, n.transform.position.z);
	rightCam.transform.position = Vector3(n.transform.position.x + messages[4] * 2.0 , n.transform.position.y, n.transform.position.z);
	leftCam.transform.rotation = n.transform.rotation;
	rightCam.transform.rotation = n.transform.rotation;
	leftCam.transform.parent = n.transform;
	rightCam.transform.parent = n.transform;
}

function nextScene() {
	Application.LoadLevelAsync(Mathf.Clamp(Application.loadedLevel + 1, 0, Application.levelCount - 1));
}

function prevScene() {
	Application.LoadLevelAsync(Mathf.Clamp(Application.loadedLevel - 1, 0, Application.levelCount - 1));
}

// function setMode(n){
// 	for(var i = 0; i < SphereMap.objAr.length; i++){
// 		SphereMap.objAr[i].GetComponent('AttractionObject').setMode(n);	
// 	}	
// }

function loadObj(n) {
	for (var i = objAr.length - 1; i >= 0; i--) {
		objAr[i].GetComponent(MeshFilter).mesh = meshes[n];
	}
}

